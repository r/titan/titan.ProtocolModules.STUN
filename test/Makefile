# This Makefile was generated by the Makefile Generator
# of the TTCN-3 Test Executor version 1.8.pre0 build 3
# for Timea Moder (etmemod@ehubuux110) on Tue May 19 10:01:38 2009

# Copyright (c) 2000-2019 Ericsson Telecom AB

# The following make commands are available:
# - make, make all     Builds the executable test suite.
# - make archive       Archives all source files.
# - make check         Checks the semantics of TTCN-3 and ASN.1 modules.
# - make clean         Removes all generated files.
# - make compile       Translates TTCN-3 and ASN.1 modules to C++.
# - make dep           Creates/updates dependency list.
# - make objects       Builds the object files without linking the executable.
# - make tags          Creates/updates tags file using ctags.

#
# Set these variables...
#

# The path of your TTCN-3 Test Executor installation:
# Uncomment this line to override the environment variable.
# TTCN3_DIR =

# Your platform: (SOLARIS, SOLARIS8, LINUX, FREEBSD or WIN32)
PLATFORM = SOLARIS8

# Your C++ compiler:
CXX = g++

# Flags for the C++ preprocessor (and makedepend as well):
CPPFLAGS = -D$(PLATFORM) -I$(OPENSSL_DIR)/include -I$(TTCN3_DIR)/include -DNO_IPV6

# Flags for the C++ compiler:
CXXFLAGS = -Wall -g

# Flags for the linker:
LDFLAGS = -g 

# Flags for the TTCN-3 and ASN.1 compiler:
COMPILER_FLAGS = -L

# Execution mode: (either ttcn3 or ttcn3-parallel)
TTCN3_LIB = ttcn3-parallel

# The path of your OpenSSL installation:
# If you do not have your own one, leave it unchanged.
OPENSSL_DIR = /mnt/TTCN/Tools/openssl-0.9.8e

# The path of your libxml2 installation:
# If you do not have your own one, leave it unchanged.
XMLDIR = $(TTCN3_DIR)

# Directory to store the archived source files:
# Note: you can set any directory except ./archive
ARCHIVE_DIR = backup

#
# You may change these variables. Add your files if necessary...
#

# TTCN-3 modules of this project:
TTCN3_MODULES = STUN_Demo.ttcn STUN_Functions.ttcn STUN_Types.ttcn

# ASN.1 modules of this project:
ASN1_MODULES =

# C++ source & header files generated from the TTCN-3 & ASN.1 modules of
# this project:
GENERATED_SOURCES = STUN_Demo.cc STUN_Functions.cc STUN_Types.cc
GENERATED_HEADERS = STUN_Demo.hh STUN_Functions.hh STUN_Types.hh

# C/C++ Source & header files of Test Ports, external functions and
# other modules:
USER_SOURCES = STUN_EncDec.cc
USER_HEADERS =

# Object files of this project that are needed for the executable test suite:
OBJECTS = STUN_Demo.o STUN_Functions.o STUN_Types.o STUN_EncDec.o

# Other files of the project (Makefile, configuration files, etc.)
# that will be added to the archived source files:
OTHER_FILES = STUN_Demo.cfg Makefile

# The name of the executable test suite:
TARGET = STUN_Demo

#
# Do not modify these unless you know what you are doing...
# Platform specific additional libraries:
#
SOLARIS_LIBS = -lsocket -lnsl -lresolv -lxnet -lxml2 
SOLARIS8_LIBS = -lsocket -lnsl -lresolv -lxnet -lxml2 
LINUX_LIBS = -lxml2 
FREEBSD_LIBS = -lxml2 
WIN32_LIBS = -lxml2 

#
# Rules for building the executable...
#

all: $(TARGET) ;

objects: $(OBJECTS) ;

$(TARGET): $(OBJECTS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJECTS) \
	-L$(TTCN3_DIR)/lib -l$(TTCN3_LIB) \
	-L$(OPENSSL_DIR)/lib -lcrypto \
	-L$(XMLDIR)/lib $($(PLATFORM)_LIBS)

.cc.o .c.o:
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) -o $@ $<

$(GENERATED_SOURCES) $(GENERATED_HEADERS): compile
	@if [ ! -f $@ ]; then rm -f compile; $(MAKE) compile; fi

check: $(TTCN3_MODULES) $(ASN1_MODULES)
	$(TTCN3_DIR)/bin/compiler -s $(COMPILER_FLAGS) \
	$(TTCN3_MODULES) $(PREPROCESSED_TTCN3_MODULES) $(ASN1_MODULES)

compile: $(TTCN3_MODULES) $(ASN1_MODULES)
	$(TTCN3_DIR)/bin/compiler $(COMPILER_FLAGS) \
	$(TTCN3_MODULES) $(ASN1_MODULES) - $?
	touch $@

browserdata.dat: $(TTCN3_MODULES) $(ASN1_MODULES)
	$(TTCN3_DIR)/bin/compiler -B -s $(COMPILER_FLAGS) \
	$(TTCN3_MODULES) $(ASN1_MODULES)

tags: $(TTCN3_MODULES) $(ASN1_MODULES) \
$(USER_HEADERS) $(USER_SOURCES)
	$(TTCN3_DIR)/bin/ctags_ttcn3 --line-directives=yes \
	$(TTCN3_MODULES) $(ASN1_MODULES) \
	$(USER_HEADERS) $(USER_SOURCES)

clean:
	-rm -f $(TARGET) $(OBJECTS) $(GENERATED_HEADERS) \
	$(GENERATED_SOURCES) compile \
	browserdata.dat tags *.log

dep: $(GENERATED_SOURCES) $(USER_SOURCES)
	makedepend $(CPPFLAGS) $(GENERATED_SOURCES) $(USER_SOURCES)

archive:
	mkdir -p $(ARCHIVE_DIR)
	tar -cvhf - $(TTCN3_MODULES) $(ASN1_MODULES) \
	$(USER_HEADERS) $(USER_SOURCES) $(OTHER_FILES) \
	| gzip >$(ARCHIVE_DIR)/`basename $(TARGET) .exe`-`date '+%y%m%d-%H%M'`.tgz

#
# Add your rules here if necessary...
#

