///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:               STUN_Functions.ttcn
//  Description:        Functions for STUN Protocol module
//  Rev:                <RnXnn>
//  Prodnr:             CNL 113 644
//  Updated:            2009-05-21
//  Contact:            http://ttcn.ericsson.se
//  Reference:          RFC 3489
///////////////////////////////////////////////
module STUN_Functions {

import from STUN_Types all;

function f_STUN_appendMappedAddress(
inout PDU_STUN pl_msg, 
in Stun_Address pl_values) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := MAPPED_ADDRESS,
		attr_length := 0,
		attr_data := {
			attr_Mapped_Address := pl_values
				}
	}  
}

function f_STUN_appendResponseAddress(
inout PDU_STUN pl_msg, 
in Stun_Address pl_values) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := RESPONSE_ADDRESS,
		attr_length := 0,
		attr_data := {
			attr_Response_Address := pl_values
				}
	}  
}

function f_STUN_appendChangedAddress(
inout PDU_STUN pl_msg, 
in Stun_Address pl_values) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := CHANGED_ADDRESS,
		attr_length := 0,
		attr_data := {
			attr_Changed_Address := pl_values
				}
	}  
}

function f_STUN_appendSourceAddress(
inout PDU_STUN pl_msg, 
in Stun_Address pl_values) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := SOURCE_ADDRESS,
		attr_length := 0,
		attr_data := {
			attr_Source_Address := pl_values
				}
	}  
}

function f_STUN_appendReflectedForm(
inout PDU_STUN pl_msg, 
in Stun_Address pl_values) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := REFLECTED_FROM,
		attr_length := 0,
		attr_data := {
			attr_Reflected_From := pl_values
				}
	}  
}

function f_STUN_appendChangeRequest(
inout PDU_STUN pl_msg, 
in Stun_Change_Request pl_request) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := CHANGE_REQUEST,
		attr_length := 0,
		attr_data := {
			attr_Change_Request := pl_request
				}
	}  
}

function f_STUN_appendUsername(
inout PDU_STUN pl_msg, 
in Stun_Username pl_username) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := USERNAME,
		attr_length := 0,
		attr_data := {
			attr_Username := pl_username
				}
	}  
}

function f_STUN_appendPassword(
inout PDU_STUN pl_msg, 
in Stun_Password pl_password) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := PASSWORD,
		attr_length := 0,
		attr_data := {
			attr_Password := pl_password
				}
	}  
}

function f_STUN_appendMessageIntegrity(
inout PDU_STUN pl_msg, 
in Stun_Message_Integrity pl_integrity := '0000000000000000000000000000000000000000'O) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := MESSAGE_INTEGRITY,
		attr_length := 0,
		attr_data := {
			attr_Message_Integrity := pl_integrity
				}
	}  
}

function f_STUN_appendErrorCode(
inout PDU_STUN pl_msg, 
in Stun_Error_Code pl_error) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := ERROR_CODE,
		attr_length := 0,
		attr_data := {
			attr_Error_Code := pl_error
				}
	}  
}

function f_STUN_appendUnkonwnAttrs(
inout PDU_STUN pl_msg, 
in Stun_Unknown_Attributes pl_unknown) 
{
	pl_msg.attr_list[sizeof(pl_msg.attr_list)] := {
	  attr_type := UNKNOWN_ATTRIBUTES,
		attr_length := 0,
		attr_data := {
			attr_Unknown_Attributes := pl_unknown
				}
	}  
}
function f_STUN_createMsgHeader(
inout PDU_STUN pl_msg, 
in STUN_Msg_Type pl_msgType,
in OCT16 pl_transId := '00000000000000000000000000000000'O)
{
  if (pl_transId == '00000000000000000000000000000000'O) {
    pl_transId := f_STUN_genTrasId();
  }

  pl_msg := {
    header := {
      msg_type := pl_msgType,
      msg_length := 0,
      trans_id := pl_transId
    },
    attr_list := {}
  }
  
}
}
